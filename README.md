## qssi-user 11 RKQ1.200826.002 V12.5.7.0.RJSCNXM release-keys
- Manufacturer: xiaomi
- Platform: lito
- Codename: gauguin
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200826.002
- Incremental: V12.5.7.0.RJSCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Redmi/gauguin/gauguin:11/RKQ1.200826.002/V12.5.7.0.RJSCNXM:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200826.002-V12.5.7.0.RJSCNXM-release-keys
- Repo: redmi_gauguin_dump_3580


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
